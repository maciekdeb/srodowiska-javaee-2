package pl.maciejdebowski.converter;

/**
 * Created by maciej.debowski on 2014-12-28.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.service.RoleService;

@Component
public class StringToRoleConverter implements Converter<String, Role> {

    @Autowired
    private RoleService roleService;

    @Override
    public Role convert(String source) {
        Role role = roleService.findByName(source.trim());
        if (role != null) {
            return role;
        } else {
            return new Role(source.trim());
        }
    }

}