package pl.maciejdebowski.converter;

/**
 * Created by maciej.debowski on 2014-12-28.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.maciejdebowski.model.HashTag;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.service.HashTagService;
import pl.maciejdebowski.service.RoleService;

@Component
public class StringToHashTagConverter implements Converter<String, HashTag> {

    @Autowired
    private HashTagService hashTagService;

    @Override
    public HashTag convert(String source) {
        HashTag hashTag = hashTagService.findByName(source.trim());
        if (hashTag != null) {
            return hashTag;
        } else {
            return new HashTag(source.trim());
        }
    }

}