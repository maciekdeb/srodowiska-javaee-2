package pl.maciejdebowski.converter;

/**
 * Created by maciej.debowski on 2014-12-28.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateToStringConverter implements Converter<Date, String> {

    private static Logger logger = LoggerFactory.getLogger(DateToStringConverter.class);

    private SimpleDateFormat createDateFormat() {
        final String format = "yyyy-MM-dd HH:mm";
        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        return dateFormat;
    }

    @Override
    public String convert(Date source) {
        logger.info("** Date to String **");
        final SimpleDateFormat dateFormat = createDateFormat();
        return dateFormat.format(source);
    }
}