package pl.maciejdebowski.converter;

/**
 * Created by maciej.debowski on 2014-12-28.
 */

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.maciejdebowski.model.HashTag;

@Component
public class HashTagToStringConverter implements Converter<HashTag, String> {

    @Override
    public String convert(HashTag source) {
        return source.getName();
    }

}