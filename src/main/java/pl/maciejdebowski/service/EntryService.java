package pl.maciejdebowski.service;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Expression;
import com.mysema.query.types.ExpressionUtils;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.Entry;
import pl.maciejdebowski.model.HashTag;
import pl.maciejdebowski.model.QEntry;
import pl.maciejdebowski.repository.EntryRepository;
import pl.maciejdebowski.repository.HashTagRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by maciek on 25.12.14.
 */
@Service
public class EntryService {

    Logger logger = LoggerFactory.getLogger(EntryService.class);

    @Autowired
    private EntryRepository entryRepository;

    @Autowired
    private HashTagRepository hashTagRepository;

    public EntryRepository getEntryRepository() {
        return entryRepository;
    }

    public void setEntryRepository(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public HashTagRepository getHashTagRepository() {
        return hashTagRepository;
    }

    public void setHashTagRepository(HashTagRepository hashTagRepository) {
        this.hashTagRepository = hashTagRepository;
    }

    public Page<Entry> getLatestEntriesAtPage(int pageNumber, int pageSize) {
        PageRequest request = new PageRequest(pageNumber, pageSize, Sort.Direction.DESC, "creationDate");
        return entryRepository.findAll(request);
    }

    public Page<Entry> getEntriesAtPage(int pageNumber, int pageSize) {
        PageRequest request = new PageRequest(pageNumber, pageSize);
        return entryRepository.findAll(request);
    }

    public List<Entry> findAll() {
        return entryRepository.findAll();
    }

    public Page<Entry> findAll(Pageable pageable) {
        return entryRepository.findAll(pageable);
    }

    public Iterable<Entry> findWithPredicates(String content, String title, String author, Set<HashTag> hashTags) {
        logger.info("*** con:" + content + " tit:" + title + " auth:" + author + " h:" + hashTags);
        QEntry entry = QEntry.entry;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if (StringUtils.isNotBlank(content)) {
            booleanBuilder.and(entry.content.contains(content));
        }
        if (StringUtils.isNotBlank(title)) {
            booleanBuilder.and(entry.title.contains(title));
        }
        if (StringUtils.isNotBlank(author)) {
            booleanBuilder.and(entry.author.username.contains(author));
        }
        if (hashTags != null && !hashTags.isEmpty()) {
            for (HashTag h : hashTags) {
                if (h.getId() == null) {
                    return null;
                }
            }
            for (HashTag h : hashTags) {
                booleanBuilder.andAnyOf(entry.hashTags.contains(h));
            }
        }

        return entryRepository.findAll(booleanBuilder.getValue());
    }

    @Transactional
    public Entry findEntryById(Long id) {
        Entry entry = entryRepository.findOne(id);
        entry.getCommentaries().size();
        entry.getHashTags().size();
        return entry;
    }

    @Transactional
    public Entry update(Entry entry) {
        logger.info("**** " + entry.getHashTags());

        if (entry.getHashTags() != null) {
            for (HashTag h : entry.getHashTags()) {
                HashTag hashTagAlreadyInDb = hashTagRepository.findByName(h.getName());
                if (hashTagAlreadyInDb != null) {
                    h.setId(hashTagAlreadyInDb.getId());
                }
            }
        }

        return entryRepository.save(entry);
    }
}
