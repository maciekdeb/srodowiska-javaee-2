package pl.maciejdebowski.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.Comment;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.repository.CommentRepository;
import pl.maciejdebowski.repository.UserRepository;

import java.util.List;

/**
 * Created by maciek on 01.12.14.
 */
@Service
public class CommentService {

    Logger logger = LoggerFactory.getLogger(CommentService.class);

    @Autowired
    private CommentRepository commentRepository;

    public Comment create(Comment comment) {
        return commentRepository.save(comment);
    }

    @Transactional
    public Comment update(Comment comment) {
//        logger.info("*** UserService.update user : " + user);
//        User userOld = findUserById(user.getId());
//
//        for (Role r : user.getRoles()) {
//            r.setId(roleRepository.findByName(r.getName()).getId());
//        }
//
//        logger.info("*** UserService.update userOld : " + userOld.getRoles());
//        logger.info("*** UserService.update user : " + user.getRoles());
//
        return commentRepository.save(comment);
    }

    public Comment getCommentById(Long commentId) {
        return commentRepository.findOne(commentId);
    }

    public Comment findById(Long commentId) {
        return commentRepository.findOne(commentId);
    }

    @Transactional
    public void markUp(Long commentId) {
        Comment comment = commentRepository.findOne(commentId);
        comment.setMark(comment.getMark() + 1);
    }

    @Transactional
    public void markDown(Long commentId) {
        Comment comment = commentRepository.findOne(commentId);
        comment.setMark(comment.getMark() - 1);
    }
}
