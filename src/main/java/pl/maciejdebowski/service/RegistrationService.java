package pl.maciejdebowski.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.model.User;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by maciek on 24.12.14.
 */
@Service
public class RegistrationService {

    Logger logger = LoggerFactory.getLogger(RegistrationService.class);

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SendMailService sendMailService;

    @Value("${application.address}")
    private String applicationAddress;

    @Transactional
    public User register(User user) {
        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        user.setUuid(UUID.randomUUID().toString());
        user.setConfirmed(Boolean.FALSE);
        user.setBanned(Boolean.FALSE);

        Set<Role> roles = new HashSet<>();
        roles.add(roleService.findByName("ROLE_USER"));
        user.setRoles(roles);

        userService.create(user);

        sendActivationMessage(user);

        return user;
    }

    public User sendActivationMessage(User user) {
        String message = "<h3>" + user.getUsername() + ", welcome to SpringBlog!</h3><p>Please use the following link to activate your account:" +
                "<a href='http://" + applicationAddress + "/registration/" + user.getUuid() + "'>Activation link</a></p>";
        String subject = "SpringBlog - Activation Account";
        sendMailService.sendMail(user.getEmail(), subject, message);
        return user;
    }

    @Transactional
    public User confirm(String confirmationId) {
        logger.info("UserService CONFIRM finding user by confirmationId : " + confirmationId);
        User user = userService.findUserByUuid(confirmationId);
        logger.info("UserService CONFIRM user : " + user);
        if (user != null) {
            user.setConfirmed(Boolean.TRUE);
            userService.update(user);
        }
        return user;
    }
}
