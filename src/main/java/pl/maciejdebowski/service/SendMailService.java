package pl.maciejdebowski.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Created by maciej.debowski on 2014-12-27.
 */
@Service
public class SendMailService {

    Logger logger = LoggerFactory.getLogger(SendMailService.class);

    @Autowired
    private JavaMailSender mailSender;

    public void sendMail(String recipient, String subject, String message) {
        try {
            MimeMessage mail = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(recipient);
            helper.setSubject(subject);
            String text = "<html><body><img src='http://www.p.lodz.pl/Images/logotype.jpg'>" + message + "</body></html>";
            helper.setText(text, true);
            mailSender.send(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
