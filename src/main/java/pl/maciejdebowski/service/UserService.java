package pl.maciejdebowski.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.model.SecurityUser;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.repository.RoleRepository;
import pl.maciejdebowski.repository.UserRepository;

import java.util.List;

/**
 * Created by maciek on 01.12.14.
 */
@Service
public class UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Page findAll(Pageable pageable) {
        Page users = userRepository.findAll(pageable);
        List<User> userList = users.getContent();
        for (User user : userList) {
            user.getRoles();
        }
        return users;
    }

    @Transactional
    public User create(User user) {
        logger.info("*** UserService.create user : " + user);
        return userRepository.save(user);
    }

    @Transactional
    public User findUserById(Long id) {
        User user = userRepository.findOne(id);
//        user.getRoles().size();
        if (user != null) {
            user.getCommentaries().size();
            user.getEntries().size();
        }
        return user;
    }

    public User login(String email, String password) {
        return userRepository.findByEmailAndPassword(email, password);
    }

    @Transactional
    public User update(User user) {
        logger.info("*** UserService.update user : " + user);
        return userRepository.save(user);
    }

    @Transactional
    public User findUserByUsername(String username) {
        logger.info("*** finding user by username=" + username);
        User user = userRepository.findByUsername(username);
        logger.info("*** found user : " + user);
        if (user != null) {
            user.getRoles().size();
        }
        return user;
    }

    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public void deleteUser(long id) {
        userRepository.delete(id);
    }

    @Transactional
    public User findUserByUuid(String confirmationId) {
        User user = userRepository.findUserByUuid(confirmationId);
        if (user != null) {
            user.getCommentaries().size();
            user.getEntries().size();
        }
        return user;
    }

    @Transactional
    public User getCurrentLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof SecurityUser) {
            username = ((SecurityUser) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return findUserByUsername(username);
    }
}
