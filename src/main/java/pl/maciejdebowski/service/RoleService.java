package pl.maciejdebowski.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.repository.RoleRepository;

import java.util.List;

/**
 * Created by maciek on 22.12.14.
 */
@Service
@Transactional
public class RoleService {

    Logger logger = LoggerFactory.getLogger(RoleService.class);

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role create(Role role) {
        return roleRepository.save(role);
    }

    public void save(Role role)
    {
        roleRepository.save(role);
    }

    public Role findByName(String name)
    {
        return roleRepository.findByName(name);
    }

    public void remove(Long id)
    {
        roleRepository.delete(id);
    }

    public void update(Role role)
    {
        logger.info("Role input in method updateNameById : " + role);
        roleRepository.updateNameById(role.getId(), role.getName());
    }
}
