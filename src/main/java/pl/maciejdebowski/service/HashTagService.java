package pl.maciejdebowski.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.Comment;
import pl.maciejdebowski.model.HashTag;
import pl.maciejdebowski.repository.CommentRepository;
import pl.maciejdebowski.repository.HashTagRepository;

/**
 * Created by maciek on 01.12.14.
 */
@Service
public class HashTagService {

    Logger logger = LoggerFactory.getLogger(HashTagService.class);

    @Autowired
    private HashTagRepository hashTagRepository;

    public HashTag create(HashTag hashTag) {
        return hashTagRepository.save(hashTag);
    }

    @Transactional
    public HashTag findByName(String name) {
        return hashTagRepository.findByName(name);
    }

}
