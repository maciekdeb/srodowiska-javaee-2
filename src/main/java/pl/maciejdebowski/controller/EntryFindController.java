package pl.maciejdebowski.controller;

import antlr.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.Entry;
import pl.maciejdebowski.model.HashTag;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.EntryService;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by maciek on 28.11.14.
 */
@Controller
public class EntryFindController {

    private static final Logger logger = LoggerFactory.getLogger(EntryFindController.class);

    @Autowired
    private EntryService entryService;

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public String entryView(@RequestParam(value = "h", required = false) HashTag hashTag,
                            @RequestParam(value = "ae", required = false) String authorUsername,
                            Model model) {
        Page<Entry> page = entryService.getEntriesAtPage(0, 10);
        model.addAttribute("content", page.getContent());
        Entry entry = new Entry();
        if (hashTag != null) {
            Set<HashTag> tagSet = new HashSet<>();
            tagSet.add(hashTag);
            entry.setHashTags(tagSet);
        }
        if (authorUsername != null) {
            User author = new User();
            author.setUsername(authorUsername);
            entry.setAuthor(author);
        }
        model.addAttribute("entryCondition", entry);
        return "entry-list";
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ModelAndView findEntry(@ModelAttribute("entryCondition") Entry entry) {
        ModelAndView modelAndView = new ModelAndView("entry-list");
        String t = entry.getTitle();
        String a = entry.getAuthor().getUsername();
        String c = entry.getContent();
        Set<HashTag> h = entry.getHashTags();
        logger.info("1.POST find: " + t + " " + a + " " + c + " " + h);
        Iterable<Entry> page = entryService.findWithPredicates(c, t, a, h);
        modelAndView.addObject("content", page);
        return modelAndView;
    }
}
