package pl.maciejdebowski.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.Comment;
import pl.maciejdebowski.model.Entry;
import pl.maciejdebowski.service.CommentService;
import pl.maciejdebowski.service.EntryService;
import pl.maciejdebowski.service.UserService;

import java.util.Date;

/**
 * Created by maciek on 28.11.14.
 */
@Controller
@SessionAttributes(value = {"entry", "newComment"})
public class EntryViewController {

    private static final Logger logger = LoggerFactory.getLogger(EntryViewController.class);

    @Autowired
    private EntryService entryService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView entryListPage() {
        Page<Entry> page = entryService.getLatestEntriesAtPage(0, 5);
        return new ModelAndView("hello", "page", page);
    }

    @RequestMapping(value = "/entry/{entryId}", method = RequestMethod.GET)
    public ModelAndView entryView(@PathVariable Long entryId,
                                  @RequestParam(value = "commentId", required = false) Long commentId,
                                  @RequestParam(value = "thumb", required = false) String thumb) {

        ModelAndView modelAndView = new ModelAndView("entry-details");

        Comment comment = new Comment();
        if (commentId != null) {
            if ("down".equals(thumb)) {
                commentService.markDown(commentId);
            } else if ("up".equals(thumb)) {
                commentService.markUp(commentId);
            } else {
                comment.setCommentReplied(commentService.getCommentById(commentId));
                modelAndView.addObject("infoAboutReplying", "You are replying to comment #" + commentId);
            }
        }
        modelAndView.addObject("newComment", comment);
        Entry entry = entryService.findEntryById(entryId);
        modelAndView.addObject("entry", entry);

        return modelAndView;
    }

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public ModelAndView entryView(/*@PathVariable Long id,*/
                                  @ModelAttribute("newComment") Comment newComment,
                                  @ModelAttribute("entry") Entry entry) {

        ModelAndView modelAndView = new ModelAndView("redirect:/entry/" + entry.getId());

        if (StringUtils.isBlank(newComment.getContent())) {
            return modelAndView;
        }

        Comment replyComment = newComment.getCommentReplied();
        if (replyComment != null) {
            String oldContent = newComment.getContent();
            String newContent = String.format("Reply to <a href='#%s'>#%s</a>: %s", replyComment.getId(), replyComment.getId(), oldContent);
            newComment.setContent(newContent);
        }
        newComment.setCreationDate(new Date());
        newComment.setMark(0);
        newComment.setEntry(entry);

        newComment.setAuthor(userService.getCurrentLoggedUser());
        entry.getCommentaries().add(newComment);
        commentService.create(newComment);
        logger.info("*** [POST] entry.getCommentaries.size: " + entry.getCommentaries().size());

        return modelAndView;
    }

}
