package pl.maciejdebowski.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.ChangeInfoForm;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.UserService;
import pl.maciejdebowski.validation.UserInfoChangeValidator;

import javax.validation.Valid;
import java.util.Locale;

/**
 * Created by maciek on 22.12.14.
 */
@Controller
@SessionAttributes
public class ChangeInfoController {

    private static final Logger logger = LoggerFactory.getLogger(ChangeInfoController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoChangeValidator userInfoChangeValidator;

    @Autowired
    private MessageSource messageSource;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(userInfoChangeValidator);
    }

    @RequestMapping(value = "/changeInfo", method = RequestMethod.GET)
    public ModelAndView changeInfoRenderPage() {
        User user = userService.getCurrentLoggedUser();
        ChangeInfoForm changeInfoForm = new ChangeInfoForm(user);
        return new ModelAndView("change-info", "changeInfoForm", changeInfoForm);
    }

    @RequestMapping(value = "/changeInfo", method = RequestMethod.POST)
    public ModelAndView editShop(@ModelAttribute("changeInfoForm") @Valid ChangeInfoForm changeInfoForm,
                                 BindingResult result,
                                 Locale locale) {

        if (result.hasErrors()) {
            return new ModelAndView("change-info");
        } else {
            User user = userService.getCurrentLoggedUser();
            user.setEmail(changeInfoForm.getEmail());
            user.setUsername(changeInfoForm.getUsername());
            userService.update(user);
            ModelAndView modelAndView = new ModelAndView("message");
            modelAndView.addObject("message", messageSource.getMessage("info.succesfullchange", null, locale));
            modelAndView.addObject("type", "great");
            return modelAndView;
        }
    }
}

