package pl.maciejdebowski.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/users")
public class UserListController {

    private static final Logger logger = LoggerFactory.getLogger(UserListController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showUsers(Model model, HttpServletRequest request) {
        String pageParam = request.getParameter("page");
        String pageSizeParam = request.getParameter("pageSize");
        int page = (StringUtils.isNotEmpty(pageParam) ? Integer.parseInt(pageParam) : 0);
        int pageSize = (StringUtils.isNotEmpty(pageSizeParam) ? Integer.parseInt(pageSizeParam) : 10);
        Pageable pageable = new PageRequest(page, pageSize);
        Page users = userService.findAll(pageable);
        model.addAttribute("userList", users.getContent());
        return "user-list";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteShop(@PathVariable Long id) {
        userService.deleteUser(id);
        return new ModelAndView("redirect:/users/list");
    }

}