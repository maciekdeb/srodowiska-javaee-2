package pl.maciejdebowski.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.RoleService;
import pl.maciejdebowski.service.UserService;

import java.util.List;

/**
 * Created by maciek on 22.12.14.
 */
@Controller
@RequestMapping("/users")
@SessionAttributes("user")
public class UserEditForm {

    private static final Logger logger = LoggerFactory.getLogger(UserListController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @InitBinder
    public void setAllowedFields(WebDataBinder binder) {
        binder.setDisallowedFields("id");
        binder.setDisallowedFields("password");
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editShopPage(@PathVariable Long id) {

        User user = userService.findUserById(id);

        logger.info("*** EditUserForm get form User.roles : " + user.getRoles());
        List<Role> roles = roleService.findAll();

        ModelAndView modelAndView = new ModelAndView("user-edit");
        modelAndView.addObject("user", user);
        modelAndView.addObject("rolesAll", roles);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView editShop(@ModelAttribute("user") /*@Valid*/ User user,
                                 BindingResult result,
                                 @PathVariable Long id) {
        if (result.hasErrors()) {
            return new ModelAndView("user-edit");
        } else {
            userService.update(user);
            return new ModelAndView("redirect:/users/list");
        }
    }


}
