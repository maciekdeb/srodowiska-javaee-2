package pl.maciejdebowski.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.Entry;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.EntryService;
import pl.maciejdebowski.service.UserService;

import java.util.Date;

/**
 * Created by maciek on 28.11.14.
 */
@Controller
@SessionAttributes("entry")
public class EntryEditController {

    private static final Logger logger = LoggerFactory.getLogger(EntryEditController.class);

    @Autowired
    private EntryService entryService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/edit/entry/{entryId}", method = RequestMethod.GET)
    public ModelAndView entryView(@PathVariable Long entryId) {
        Entry entry = entryService.findEntryById(entryId);
        ModelAndView modelAndView = new ModelAndView("entry-edit");
        modelAndView.addObject("entry", entry);
        modelAndView.addObject("buttonSubmitValue", "Save changes");
        modelAndView.addObject("formAction", "/edit/entry/");
        return modelAndView;
    }

    @RequestMapping(value = "/edit/entry/", method = RequestMethod.POST)
    public ModelAndView entryView(@ModelAttribute("entry") Entry entry) {
        entryService.update(entry);
        return new ModelAndView("redirect:/entry/" + entry.getId());
    }

    @RequestMapping(value = "/create/entry", method = RequestMethod.GET)
    public ModelAndView createEntryView() {
        ModelAndView modelAndView = new ModelAndView("entry-create");
        modelAndView.addObject("entry", new Entry());
        modelAndView.addObject("buttonSubmitValue", "Create");
        modelAndView.addObject("formAction", "/create/entry");
        return modelAndView;
    }

    @RequestMapping(value = "/create/entry", method = RequestMethod.POST)
    public ModelAndView createEntryView(@ModelAttribute("entry") Entry entry) {

        User user = userService.getCurrentLoggedUser();
        entry.setAuthor(user);
        entry.setCreationDate(new Date());
        entryService.update(entry);
        return new ModelAndView("redirect:/entry/" + entry.getId());
    }

}
