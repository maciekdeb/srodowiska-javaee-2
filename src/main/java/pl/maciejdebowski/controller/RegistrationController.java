package pl.maciejdebowski.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.RegistrationService;
import pl.maciejdebowski.validation.UserRegistrationValidator;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.Locale;

/**
 * Created by maciek on 28.11.14.
 */
@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private UserRegistrationValidator userRegistrationValidator;

    @Autowired
    private MessageSource messageSource;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(userRegistrationValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView viewRegistrationForm() {
        return new ModelAndView("sign-up", "user", new User());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView register(@ModelAttribute(value = "user") @Valid User user,
                                 BindingResult result,
                                 Locale locale) throws MessagingException {
        if (result.hasErrors()) {
            return new ModelAndView("sign-up");
        } else {
            registrationService.register(user);
            logger.info("New user " + user.toString() + " was successfully created.");
            ModelAndView modelAndView = new ModelAndView("message");
            modelAndView.addObject("message", messageSource.getMessage("registration.info", null, locale));
            modelAndView.addObject("type", "msg");
            return modelAndView;
        }
    }

    @RequestMapping(value = "/{confirmationId}", method = RequestMethod.GET)
    public ModelAndView confirmationForm(@PathVariable("confirmationId") String confirmationId,
                                         RedirectAttributes redirectAttributes,
                                         Locale locale) {
        registrationService.confirm(confirmationId);
        redirectAttributes.addFlashAttribute("msg", messageSource.getMessage("registration.login", null, locale));
        return new ModelAndView("redirect:/login");
    }

}
