package pl.maciejdebowski.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;

/**
 * Created by maciek on 28.11.14.
 */
@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
            Locale locale) {

        ModelAndView model = new ModelAndView("login");
        if (error != null) {
            model.addObject("error", messageSource.getMessage("login.invalid", null, locale));
        }
        return model;
    }

    @RequestMapping(value = "/403")
    public ModelAndView error(Locale locale) {
        ModelAndView modelAndView = new ModelAndView("message");

        modelAndView.addObject("type", "error");
        modelAndView.addObject("message", messageSource.getMessage("403.message", null, locale));

        return modelAndView;
    }

}
