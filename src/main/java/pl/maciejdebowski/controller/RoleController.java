package pl.maciejdebowski.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.Role;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.RoleService;
import pl.maciejdebowski.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "/roles")
@SessionAttributes("role")
public class RoleController {

    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView showRoles() {
        List<Role> roles = roleService.findAll();
        return new ModelAndView("role-list", "roles", roles);
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createRole() {
        ModelAndView modelAndView = new ModelAndView("role-details");
        modelAndView.addObject("submitActionPage", "/roles/create");
        modelAndView.addObject("role", new Role());
        return modelAndView;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createRole(@ModelAttribute /*@Valid*/ Role role,
                                   BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("role-details");
        } else {
            roleService.create(role);
            return new ModelAndView("redirect:/roles/list");
        }
    }

    @RequestMapping(value = "/edit/{name}", method = RequestMethod.GET)
    public ModelAndView editRole(@PathVariable String name) {
        Role role = roleService.findByName(name);

        ModelAndView modelAndView = new ModelAndView("role-details");
        modelAndView.addObject("submitActionPage", "/roles/edit");
        modelAndView.addObject("role", role);

        logger.info("*** editing role : " + role);
        return modelAndView;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editRole(@ModelAttribute /*@Valid*/ Role role,
                                 BindingResult result) {
        logger.info("*** role was editted : " + role);
        if (result.hasErrors()) {
            return new ModelAndView("role-details");
        } else {
            System.out.println(role);
            roleService.update(role);
            return new ModelAndView("redirect:/roles/list");
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteRole(@PathVariable Long id) {
        roleService.remove(id);
        return new ModelAndView("redirect:/roles/list");
    }

}