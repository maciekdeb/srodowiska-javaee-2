package pl.maciejdebowski.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import pl.maciejdebowski.model.ChangePasswordForm;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.UserService;
import pl.maciejdebowski.validation.ChangePasswordFormValidator;

import java.util.Locale;

/**
 * Created by maciek on 22.12.14.
 */
@Controller
@SessionAttributes
public class ChangePasswordController {

    private static final Logger logger = LoggerFactory.getLogger(ChangePasswordController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public ModelAndView changePasswordRenderPage() {
        return new ModelAndView("change-password", "changePasswordForm", new ChangePasswordForm());
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ModelAndView editShop(@ModelAttribute("changePassword") ChangePasswordForm changePasswordForm,
                                 BindingResult result,
                                 Locale locale) {

        User user = userService.getCurrentLoggedUser();
        new ChangePasswordFormValidator(user).validate(changePasswordForm, result);
        if (result.hasErrors()) {
            return new ModelAndView("change-password");
        } else {
            user.setPassword(BCrypt.hashpw(changePasswordForm.getNewPassword(), BCrypt.gensalt()));
            userService.update(user);
            ModelAndView modelAndView = new ModelAndView("message");
            modelAndView.addObject("message", messageSource.getMessage("change.password.success", null, locale));
            modelAndView.addObject("type", "great");
            return modelAndView;
        }
    }
}

