package pl.maciejdebowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.maciejdebowski.model.Comment;

/**
 * Created by maciek on 01.12.14.
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {

}