package pl.maciejdebowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.maciejdebowski.model.HashTag;
import pl.maciejdebowski.model.User;

/**
 * Created by maciek on 01.12.14.
 */
public interface HashTagRepository extends JpaRepository<HashTag, Long> {
    HashTag findByName(String name);
}