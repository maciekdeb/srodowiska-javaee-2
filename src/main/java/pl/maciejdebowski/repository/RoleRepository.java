package pl.maciejdebowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.maciejdebowski.model.Role;

import java.io.Serializable;

/**
 * Created by maciek on 01.12.14.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Modifying(clearAutomatically = true)
    @Query("update Role role set role.name =:name where role.id =:id")
    void updateNameById(@Param("id") Long id, @Param("name") String name);

}