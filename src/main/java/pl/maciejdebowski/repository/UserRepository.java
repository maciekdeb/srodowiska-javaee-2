package pl.maciejdebowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pl.maciejdebowski.model.User;

/**
 * Created by maciek on 01.12.14.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByEmailAndPassword(String email, String password);
    User findUserByEmail(String email);
    User findUserByUuid(String uuid);
}