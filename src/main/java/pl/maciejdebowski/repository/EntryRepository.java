package pl.maciejdebowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import pl.maciejdebowski.model.Entry;
import pl.maciejdebowski.model.HashTag;

/**
 * Created by maciek on 01.12.14.
 */
public interface EntryRepository extends JpaRepository<Entry, Long>, QueryDslPredicateExecutor<Entry> {

}