package pl.maciejdebowski.validation;

/**
 * Created by maciej.debowski on 2014-12-26.
 */

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.maciejdebowski.model.ChangePasswordForm;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.UserService;

/**
 * Created by maciej.debowski on 2014-12-26.
 */
public class ChangePasswordFormValidator implements Validator {

    private User user;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    public ChangePasswordFormValidator(User user) {
        this.user = user;
    }

    public void validate(Object obj, Errors errors) {
        ChangePasswordForm form = (ChangePasswordForm) obj;
        if (StringUtils.isNotBlank(form.getNewPassword())) {
            if (!BCrypt.checkpw(form.getOriginalPassword(), user.getPassword())) {
                errors.rejectValue("originalPassword", "error.original.password.mismatch");
            }
        }
    }
}

