package pl.maciejdebowski.validation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.maciejdebowski.model.ChangeInfoForm;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.UserService;

@Component
public class UserInfoChangeValidator implements Validator {

    Logger logger = LoggerFactory.getLogger(UserInfoChangeValidator.class);

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return ChangeInfoForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChangeInfoForm userDetails = (ChangeInfoForm) target;

        ValidationUtils.rejectIfEmpty(errors, "username", "userForm.username.empty");

        if (StringUtils.isBlank(userDetails.getEmail())) {
            errors.rejectValue("email", "userForm.email.empty");
        } else {
            EmailValidator emailValidator = new EmailValidator();
            boolean valid = emailValidator.validate(userDetails.getEmail());
            if (!valid) {
                errors.rejectValue("email", "userForm.email.notvalid");
            }
        }

        User currentUser = userService.getCurrentLoggedUser();
        User findByMail = userService.findUserByEmail(userDetails.getEmail());
        if (findByMail != null) {
            if (findByMail.equals(currentUser.getEmail())) {
                errors.rejectValue("email", "userForm.email.alreadyExists");
            }
        }
        User findByUsername = userService.findUserByUsername(userDetails.getUsername());
        if (findByUsername != null) {
            if (findByUsername.equals(currentUser.getUsername())) {
                errors.rejectValue("username", "userForm.username.alreadyExists");
            }
        }
    }

}
