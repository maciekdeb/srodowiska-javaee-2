package pl.maciejdebowski.validation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.service.UserService;

@Component
public class UserRegistrationValidator implements Validator {

    Logger logger = LoggerFactory.getLogger(UserRegistrationValidator.class);

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User userForm = (User) target;

        ValidationUtils.rejectIfEmpty(errors, "username", "userForm.username.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "userForm.password.empty");
        if (StringUtils.isEmpty(userForm.getEmail())) {
            errors.rejectValue("email", "userForm.email.empty");
        } else {
            EmailValidator emailValidator = new EmailValidator();
            boolean valid = emailValidator.validate(userForm.getEmail());
            if (!valid) {
                errors.rejectValue("email", "userForm.email.notvalid");
            }
        }
        if (userService.findUserByEmail(userForm.getEmail()) != null) {
            errors.rejectValue("email", "userForm.email.alreadyExists");
        }
        if (userService.findUserByUsername(userForm.getUsername()) != null) {
            errors.rejectValue("username", "userForm.username.alreadyExists");
        }
        if (userForm.getPassword().length() < 6 || userForm.getPassword().length() > 15) {
            errors.rejectValue("password", "userForm.password.wrongNumber");
        }
        if (!StringUtils.isEmpty(userForm.getPassword())) {
            if (!userForm.getPassword().equals(userForm.getPasswordConfirmation())) {
                errors.rejectValue("password", "userForm.password.mismatch");
                errors.rejectValue("passwordConfirmation", "userForm.passwordConfirmation.mismatch");
            }
        }

    }

}
