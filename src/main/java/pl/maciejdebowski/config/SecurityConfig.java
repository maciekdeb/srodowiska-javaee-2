package pl.maciejdebowski.config;

/**
 * Created by maciek on 27.11.14.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import pl.maciejdebowski.service.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
//@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("a").password("a").roles("USER");
        auth.userDetailsService(customUserDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Bean
    public SimpleUrlAuthenticationSuccessHandler simpleUrlAuthenticationSuccessHandler() {
        return new SimpleUrlAuthenticationSuccessHandler();
    }

    @Autowired
    public SimpleUrlAuthenticationSuccessHandler simpleUrlAuthenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling().accessDeniedPage("/403").and()
                .authorizeRequests()
                    .antMatchers(
                            "/registration**",
                            "/query**",
                            "/login**",
                            "/entry**",
                            "/"
                        ).permitAll()
                    .antMatchers(
                            "/changeInfo",
                            "/changePassword",
                            "/addComment"
                        ).hasRole("USER")
                    .antMatchers(
                            "/edit/**",
                            "/create/**"
                        ).hasRole("EDITOR")
                    .antMatchers(
                            "/roles/**",
                            "/users/**"
                        ).hasRole("ADMIN")
                .and()
                    .formLogin().loginPage("/login").failureUrl("/login?error").successHandler(simpleUrlAuthenticationSuccessHandler)
                .and()
                .logout().logoutSuccessUrl("/")
                .and()
                .csrf();

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
}