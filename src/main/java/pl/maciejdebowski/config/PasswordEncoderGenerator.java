package pl.maciejdebowski.config;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.UUID;

/**
 * Created by maciej.debowski on 2014-12-22.
 */
public class PasswordEncoderGenerator {

    public static void main(String[] args) {

        for (int i = 0; i < 100; i++) {
            String uuid = UUID.randomUUID().toString();
            System.out.println(uuid + " " + uuid.length());

        }

//        int i = 0;
//        while (i < 10) {
//            String password = "123456";
//            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//            String hashedPassword = passwordEncoder.encode(password);
//
//            System.out.println(hashedPassword);
//            i++;
//        }

    }

}
