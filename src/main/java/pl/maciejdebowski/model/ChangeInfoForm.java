package pl.maciejdebowski.model;

/**
 * Created by maciej.debowski on 2014-12-26.
 */
public class ChangeInfoForm {

    private String email;
    private String username;

    public ChangeInfoForm() {
    }

    public ChangeInfoForm(String email, String username) {
        this.email = email;
        this.username = username;
    }

    public ChangeInfoForm(User user) {
        this.email = user.getEmail();
        this.username = user.getUsername();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
