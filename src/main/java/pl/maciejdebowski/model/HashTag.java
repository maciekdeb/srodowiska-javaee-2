package pl.maciejdebowski.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by maciek on 23.12.14.
 */
@Entity
@Table(schema = "public", name = "`HASH_TAG`")
public class HashTag implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hash_tag_seq")
    @SequenceGenerator(name = "hash_tag_seq", sequenceName = "hash_tag_seq")
    private Long id;

    @Column(nullable = false)
    private String name;

    public HashTag() {
    }

    public HashTag(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HashTag hashTag = (HashTag) o;

        if (id != null ? !id.equals(hashTag.id) : hashTag.id != null) return false;
        if (name != null ? !name.equals(hashTag.name) : hashTag.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HashTag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
