package pl.maciejdebowski.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * Created by maciek on 01.12.14.
 */
public class SecurityUser extends User implements UserDetails {

    private static final long serialVersionUID = 1L;

    Logger logger = LoggerFactory.getLogger(SecurityUser.class);

    public SecurityUser(User user) {
        if (user != null) {
            logger.info("*** New Security User: " + user);
            this.setId(user.getId());
            this.setUsername(user.getUsername());
            this.setEmail(user.getEmail());
            this.setPassword(user.getPassword());
            this.setJoinDate(user.getJoinDate());
            this.setRoles(user.getRoles());
            this.setBanned(user.getBanned());
            this.setConfirmed(user.getConfirmed());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        Set<Role> userRoles = this.getRoles();
        if (userRoles != null) {
            for (Role role : userRoles) {
                logger.info("*** Listing user roles: " + role);
                SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getName());
                authorities.add(authority);
            }
        }

        return authorities;
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !super.getBanned();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return super.getConfirmed();
    }

}
