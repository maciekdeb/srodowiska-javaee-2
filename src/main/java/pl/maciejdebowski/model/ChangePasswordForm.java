package pl.maciejdebowski.model;

/**
 * Created by maciej.debowski on 2014-12-26.
 */
public class ChangePasswordForm {

    private String originalPassword;
    private String newPassword;
    private String newPasswordConfirmation;

    public ChangePasswordForm() {
    }

    public ChangePasswordForm(String originalPassword, String newPassword, String newPasswordConfirmation) {
        this.originalPassword = originalPassword;
        this.newPassword = newPassword;
        this.newPasswordConfirmation = newPasswordConfirmation;
    }

    public String getOriginalPassword() {
        return originalPassword;
    }

    public void setOriginalPassword(String originalPassword) {
        this.originalPassword = originalPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirmation() {
        return newPasswordConfirmation;
    }

    public void setNewPasswordConfirmation(String newPasswordConfirmation) {
        this.newPasswordConfirmation = newPasswordConfirmation;
    }
}
