package pl.maciejdebowski.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by maciek on 01.12.14.
 */
@Entity
@Table(schema = "public", name = "`USER`")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq")
    private Long id;

    @Column(length = 36)
    private String uuid;

    private String username;

    @Column(unique = true)
    private String email;

    @Transient
    private String emailConfirmation;

    private String password;

    @Transient
    private String passwordConfirmation;

    private Boolean confirmed;

    private Boolean banned;

    private Date joinDate;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "`USER_ROLE`",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private Set<Entry> entries;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private Set<Comment> commentaries;

    public User() {
    }

    public User(Long id, String uuid, String userName, String email, String password, Boolean confirmed, Date joinDate, Boolean banned, Set<Entry> entries, Set<Comment> commentaries) {
        this.uuid = uuid;
        this.confirmed = confirmed;
        this.entries = entries;
        this.id = id;
        this.username = userName;
        this.email = email;
        this.password = password;
        this.joinDate = joinDate;
        this.banned = banned;
        this.commentaries = commentaries;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", emailConfirmation='" + emailConfirmation + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", confirmed=" + confirmed +
                ", banned=" + banned +
                ", joinDate=" + joinDate +
                ", roles=" + roles +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public String getEmailConfirmation() {
        return emailConfirmation;
    }

    public void setEmailConfirmation(String emailConfirmation) {
        this.emailConfirmation = emailConfirmation;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public Set<Entry> getEntries() {
        return entries;
    }

    public void setEntries(Set<Entry> entries) {
        this.entries = entries;
    }

    public Set<Comment> getCommentaries() {
        return commentaries;
    }

    public void setCommentaries(Set<Comment> commentaries) {
        this.commentaries = commentaries;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}