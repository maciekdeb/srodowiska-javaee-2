<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"><spring:message code="navbar.toogglenavigation"/></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><spring:message code="navbar.brandlogo"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/"><spring:message code="navbar.home"/></a></li>
                <li><a href="/query"><spring:message code="navbar.find"/> <i class="glyphicon glyphicon-search"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><spring:message code="navbar.language"/><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="?language=pl">pl</a></li>
                        <li><a href="?language=en">en</a></li>
                    </ul>
                </li>
                <sec:authorize access="isAuthenticated()">
                    <sec:authorize access="hasRole('ROLE_EDITOR')">
                        <li><a href="/create/entry"><spring:message code="navbar.addpost"/></a></li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><spring:message code="navbar.accessmanagement"/><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="${pageContext.request.contextPath}/users/list"><spring:message code="navbar.accessmanagement.users"/></a></li>
                                <li><a href="${pageContext.request.contextPath}/roles/list"><spring:message code="navbar.accessmanagement.roles"/></a></li>
                            </ul>
                        </li>
                    </sec:authorize>
                </sec:authorize>
                <sec:authorize access="isAnonymous()">
                    <li>
                        <div class="dropdown">
                            <button type="button" class="btn btn-default navbar-btn" data-toggle="dropdown"><spring:message code="navbar.login"/></button>
                            <div class="dropdown-menu" style="width: 200px; padding: 10px; background: #ddd">
                                <form name="loginForm" action="<c:url value='/login' />" method="POST" role="form">
                                    <div class="form-group m">
                                        <input type="text" name="username" class="form-control" id="user"
                                               placeholder="User"/>
                                        <input type="password" name="password" class="form-control" id="password"
                                               placeholder="Password"/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    </div>
                                    <a href="/registration" role="button" class="btn btn-info" ><spring:message code="navbar.signup"/></a>
                                    <button type="submit" class="btn btn-success"><spring:message code="navbar.signin"/></button>
                                </form>
                            </div>
                        </div>
                    </li>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <sec:authentication property="principal.username" />
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/changeInfo"><spring:message code="navbar.changeinfo"/></a></li>
                            <li><a href="/changePassword"><spring:message code="navbar.changepassword"/></a></li>
                            <li class="divider"></li>
                            <li>
                                <form action="<c:url value="/logout" />" method="post" id="logoutForm">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    <input type="submit" value="<spring:message code="navbar.logout"/>" class="btn btn-danger btn-sm btn-block">
                                </form>
                            </li>
                        </ul>
                    </li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</nav>