<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="jumbotron">
  <h1><spring:message code="index.title"/></h1>
  <p><spring:message code="index.content"/></p>
  <p>
    <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button"><spring:message code="index.link"/></a>
  </p>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <table class="table table-condensed table-hover">
      <thead>
        <tr><td><spring:message code="index.entry.list.head"/></td></tr>
      </thead>
      <tbody>
      <c:forEach var="entry" items="${page.content}">
        <tr>
          <td><a href="${pageContext.request.contextPath}/entry/${entry.id}">${entry.title}</a></td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
</div>