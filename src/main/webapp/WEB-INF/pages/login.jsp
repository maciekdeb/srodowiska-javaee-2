<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty error}">
    <div class="error">${error}</div>
</c:if>
<c:if test="${not empty msg}">
    <div class="msg">${msg}</div>
</c:if>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading"><strong class=""><spring:message code="login.head.login"/></strong>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="<c:url value='/login' />" method="POST" role="form">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label"><spring:message code="login.username"/></label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inputEmail3" name="username" required="" type="username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label"><spring:message code="login.password"/></label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inputPassword3" name="password" required="" type="password">
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm"><spring:message code="login.signin"/></button>
                                <button type="reset" class="btn btn-default btn-sm"><spring:message code="login.reset"/></button>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </div>
                <div class="panel-footer"><a href="/registration" class=""><spring:message code="login.register"/></a></div>
            </div>
        </div>
    </div>
</div>