<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <td><strong><spring:message code="user.list.id"/></strong></td>
                    <td><strong><spring:message code="user.list.username"/></strong></td>
                    <td><strong><spring:message code="user.list.email"/></strong></td>
                    <td><strong><spring:message code="user.list.confirmed"/></strong></td>
                    <td><strong><spring:message code="user.list.locked"/></strong></td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="user" items="${userList}">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.username}</td>
                        <td>${user.email}</td>
                        <td>
                            <c:choose>
                                <c:when test="${user.confirmed}">
                                    <span class="text-success">${user.confirmed}</span>
                                </c:when>
                                <c:otherwise>
                                    <span class="text-danger">${user.confirmed}</span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${user.banned}">
                                    <span class="text-success">${user.banned}</span>
                                </c:when>
                                <c:otherwise>
                                    <span class="text-danger">${user.banned}</span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <a href="${pageContext.request.contextPath}/users/edit/${user.id}">
                                <spring:message code="user.list.edit"/>
                            </a><br/>
                            <a class="text-danger" href="${pageContext.request.contextPath}/users/delete/${user.id}">
                                <spring:message code="user.list.delete"/>
                            </a><br/>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>