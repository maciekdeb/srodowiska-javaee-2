<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form class="form-horizontal" action='${pageContext.request.contextPath}/changePassword' method="POST" modelAttribute="changePasswordForm" commandName="changePasswordForm">
    <fieldset>
        <div id="legend">
            <legend class=""><spring:message code="form.change.password.legend"/></legend>
        </div>
        <div class="control-group">
            <label class="control-label" for="originalPassword"><spring:message code="form.change.password.originalPassword"/></label>
            <div class="controls">
                <form:password path="originalPassword" size="30" id="originalPassword" cssClass="input-xlarge"/>
                <form:errors path="originalPassword" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="newPassword"><spring:message code="form.change.password.newPassword"/></label>
            <div class="controls">
                <form:password path="newPassword" size="30" id="newPassword" cssClass="input-xlarge"/>
                <form:errors path="newPassword" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="newPasswordConfirmation"><spring:message code="form.change.password.newPasswordConfirmation"/></label>
            <div class="controls">
                <form:password path="newPasswordConfirmation" size="30" id="newPasswordConfirmation" cssClass="input-xlarge"/>
                <form:errors path="newPasswordConfirmation" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="<spring:message code="form.change.password.submit"/>" class="btn btn-success"/>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form:form>

