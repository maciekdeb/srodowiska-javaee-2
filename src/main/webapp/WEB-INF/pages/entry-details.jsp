<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="panel panel-default">
    <div class="panel-body">
        <div id="legend">
            <legend>
                ${entry.title}
                <sec:authorize access="hasRole('ROLE_EDITOR')">
                    | <a href="/edit/entry/${entry.id}"><spring:message code="form.entry.details.editpost"/></a>
                </sec:authorize>
            </legend>
        </div>
        <div style="text-align: justify;">
            <span>${entry.content}</span>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-9">
                <c:forEach var="hashTag" items="${entry.hashTags}">
                    <a href="${pageContext.request.contextPath}/query?h=${hashTag.name}"><span class="label label-default">#${hashTag.name}</span></a>
                </c:forEach>
            </div>
            <div class="col-md-3"><a href="${pageContext.request.contextPath}/query?a=${entry.author.username}">${entry.author.username}</a> | <spring:eval expression="entry.creationDate"/></div>
        </div>
        <div class="row">
            <div class="col-md-offset-5">


            </div>
        </div>
    </div>
</div>

<sec:authorize access="hasRole('ROLE_USER')">
    <form:form class="form-horizontal" action='${pageContext.request.contextPath}/addComment'
               method="POST"
               commandName="newComment">
        <div class="control-group container panel-body">
                ${infoAboutReplying}
            <div class="controls">
                <div class="row">
                    <div class="col-sm-6"><form:textarea path="content" id="content" cssClass="input-xlarge"
                                                         cssStyle="height: 70px; width: 100%"/>
                    </div>
                    <div class="col-sm-3"><input type="submit"
                                                 value="<spring:message code="form.entry.details.addComment.submit"/>"
                                                 class="btn btn-success"
                                                 style="height: 70px"/>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form:form>
</sec:authorize>
<c:forEach var="comment" items="${entry.commentaries}">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <a id="${comment.id}" style="">#${comment.id}</a> ${comment.author.username} <spring:message
                        code="form.entry.details.comment.inDay"/> <spring:eval expression="comment.creationDate"/>
                    <spring:message code="form.entry.details.comment.wrote"/>:
                </div>
                <div class="col-md-1">
                        ${comment.mark}
                    <strong>
                        <a href="/entry/${entry.id}?commentId=${comment.id}&thumb=down" class="text-danger"><i
                                class="glyphicon glyphicon-thumbs-down"></i></a>
                    </strong>
                    <strong>
                        <a href="/entry/${entry.id}?commentId=${comment.id}&thumb=up" class="text-success"><i
                                class="glyphicon glyphicon-thumbs-up"></i></a>
                    </strong>
                </div>
                <div class="col-md-1">
                    <a href="/entry/${entry.id}?commentId=${comment.id}"><spring:message
                            code="form.entry.details.comment.reply"/></a>
                </div>
            </div>
            <span>${comment.content}</span>
        </div>
    </div>
</c:forEach>
