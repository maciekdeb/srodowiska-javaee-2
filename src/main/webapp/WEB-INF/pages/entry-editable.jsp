<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="panel panel-default">
    <div class="panel-body">
        <form:form class="form-horizontal" action='${pageContext.request.contextPath}${formAction}'
                   method="POST"
                   commandName="entry">
            <div id="legend">
                <spring:message code="form.entry.editable.title" var="legendTitle"/>
                <legend><form:input path="title" cssStyle="width: 100%" placeholder="${legendTitle}"/></legend>
            </div>
            <div style="text-align: justify;">
                <spring:message code="form.entry.editable.content" var="contentPlaceholder"/>
                <form:textarea path="content" cssStyle="width: 100%;" rows="20" placeholder="${contentPlaceholder}"></form:textarea>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-9">
                    <spring:message code="form.entry.editable.hashTags" var="hashPlaceholder"/>
                    <form:input path="hashTags" cssStyle="width: 100%;" placeholder="${hashPlaceholder}"/>
                </div>
                <div class="col-md-3">
                    <input type="submit" value="${buttonSubmitValue}" class="btn form-control btn-success"/>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form:form>
    </div>
</div>