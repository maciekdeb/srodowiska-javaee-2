<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form class="form-horizontal" action='/registration' method="POST" commandName="user">
    <fieldset>
        <div id="legend">
            <legend><spring:message code="signup.legend"/></legend>
        </div>
        <div class="control-group">
            <label class="control-label" for="username">
                <spring:message code="signup.username"/>
            </label>
            <div class="controls">
                <form:input path="username" size="30" id="username" cssClass="input-xlarge"/>
                <p class="help-block">
                    <spring:message code="signup.username.info"/>
                </p>
                <form:errors path="username" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="email">
                <spring:message code="signup.email"/>
            </label>
            <div class="controls">
                <form:input path="email" size="30" id="email" cssClass="input-xlarge"/>
                <p class="help-block">
                    <spring:message code="signup.email.info"/>
                </p>
                <form:errors path="email" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="password">
                <spring:message code="signup.password"/>
            </label>
            <div class="controls">
                <form:password path="password" size="30" id="password" cssClass="input-xlarge"/>
                <p class="help-block">
                    <spring:message code="signup.password.info"/>
                </p>
                <form:errors path="password" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="passwordConfirmation">
                <spring:message code="signup.passwordConfirmation"/>
            </label>
            <div class="controls">
                <form:password path="passwordConfirmation" size="30" id="passwordConfirmation" cssClass="input-xlarge"/>
                <p class="help-block">
                    <spring:message code="signup.passwordConfirmation.info"/>
                </p>
                <form:errors path="passwordConfirmation" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="<spring:message code="signup.submit"/>" class="btn btn-success"/>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form:form>