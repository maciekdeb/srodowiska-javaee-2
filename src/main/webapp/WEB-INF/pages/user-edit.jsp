<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form class="form-horizontal" action='${pageContext.request.contextPath}/users/edit/${user.id}' method="POST" modelAttribute="user" commandName="user">
    <fieldset>
        <div id="legend">
            <legend><spring:message code="user.edit.legend"/></legend>
        </div>
        <div class="control-group">
            <label class="control-label" for="username"><spring:message code="user.edit.username"/></label>
            <div class="controls">
                <form:input path="username" size="30" id="username" cssClass="input-xlarge"/>
                <form:errors path="username" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="email"><spring:message code="user.edit.email"/></label>
            <div class="controls">
                <form:input path="email" size="30" id="email" cssClass="input-xlarge"/>
                <form:errors path="email" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="joinDate"><spring:message code="user.edit.joindate"/></label>
            <div class="controls">
                <form:input path="joinDate" size="30" id="joinDate" cssClass="input-xlarge"/>
                <form:errors path="joinDate" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="roles"><spring:message code="user.edit.roles"/></label>
            <div class="controls">
                <form:select multiple="true" path="roles">
                    <form:options items="${rolesAll}" itemValue="name" itemLabel="name"/>
                </form:select>
                <form:errors path="roles" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="confirmed"><spring:message code="user.edit.confirmed"/></label>
            <div class="controls">
                <form:checkbox path="confirmed" size="30" id="confirmed" cssClass="input-xlarge"/>
                <form:errors path="confirmed" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="banned"><spring:message code="user.edit.banned"/></label>
            <div class="controls">
                <form:checkbox path="banned" size="30" id="banned" cssClass="input-xlarge"/>
                <form:errors path="banned" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="<spring:message code="user.edit.submit"/>" class="btn btn-success"/>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form:form>

