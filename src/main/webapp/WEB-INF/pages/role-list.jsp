<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <td><strong><spring:message code="role.list.identificator"/></strong></td>
                    <td><strong><spring:message code="role.list.name"/></strong></td>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="role" items="${roles}">
                    <tr>
                        <td>${role.id}</td>
                        <td>${role.name}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/roles/edit/${role.name}"><spring:message code="role.list.edit"/></a><br/>
                            <a class="text-danger"
                               href="${pageContext.request.contextPath}/roles/delete/${role.id}"><spring:message code="role.list.delete"/></a><br/>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>