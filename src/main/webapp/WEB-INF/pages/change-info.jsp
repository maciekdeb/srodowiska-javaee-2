<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form class="form-horizontal" action='${pageContext.request.contextPath}/changeInfo' method="POST"
           modelAttribute="changeInfoForm" commandName="changeInfoForm">
    <fieldset>
        <div id="legend">
            <legend class=""><spring:message code="form.change.info.legend"/></legend>
        </div>
        <div class="control-group">
            <label class="control-label" for="email"><spring:message code="form.change.info.email"/></label>

            <div class="controls">
                <form:input path="email" size="30" id="email" cssClass="input-xlarge"/>
                <form:errors path="email" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="username"><spring:message code="form.change.info.username"/></label>

            <div class="controls">
                <form:input path="username" size="30" id="username" cssClass="input-xlarge"/>
                <form:errors path="username" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="<spring:message code="form.change.info.submit"/>" class="btn btn-success"/>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form:form>

