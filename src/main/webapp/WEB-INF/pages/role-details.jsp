<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form class="form-horizontal" action='${pageContext.request.contextPath}${submitActionPage}' method="POST" commandName="role">
    <fieldset>
        <div id="legend">
            <legend><spring:message code="role.details.legend"/></legend>
        </div>
        <div class="control-group">
            <label class="control-label" for="name"><spring:message code="role.details.name"/></label>
            <div class="controls">
                <form:input path="name" size="30" id="name" cssClass="input-xlarge"/>
                <form:errors path="name" cssClass="text-danger"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="<spring:message code="role.details.submit"/>" class="btn btn-success"/>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form:form>

