<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="panel panel-default">
    <div class="panel-body">
        <form:form class="form-horizontal" action='${pageContext.request.contextPath}/query'
                   method="POST" modelAttribute="entryCondition" commandName="entryCondition">
            <fieldset>
                <div id="legend">
                    <legend class=""><spring:message code="form.entry.list.legend"/></legend>
                </div>
                <div class="control-group">
                    <label class="control-label" for="title"><spring:message code="form.entry.list.title"/></label>
                    <div class="controls">
                        <form:input path="title" size="30" id="title" cssClass="form-control input-xlarge"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="content"><spring:message code="form.entry.list.content"/></label>
                    <div class="controls">
                        <form:input path="content" size="30" id="content" cssClass="form-control input-xlarge"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="author.username"><spring:message code="form.entry.list.author"/></label>
                    <div class="author">
                        <form:input path="author.username" size="30" id="author.username" cssClass="form-control input-xlarge"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="hashTags"><spring:message code="form.entry.list.hashTags"/></label>
                    <div class="author">
                            <form:input path="hashTags" cssStyle="width: 100%;"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls" style="padding-top: 10px; text-align:right">
                        <input type="submit" value="<spring:message code="form.entry.list.submit"/>" class="btn form-control btn-success"/>
                    </div>
                </div>
            </fieldset>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form:form>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-condensed table-hover">
            <thead>
                <spring:message code="form.entry.list.head"/>
            </thead>
            <tbody>
            <c:forEach var="entry" items="${content}">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/entry/${entry.id}">${entry.title}</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>