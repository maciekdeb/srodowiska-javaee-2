package pl.maciejdebowski;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.maciejdebowski.config.PersistenceConfig;
import pl.maciejdebowski.model.Entry;
import pl.maciejdebowski.model.User;
import pl.maciejdebowski.repository.EntryRepository;
import pl.maciejdebowski.repository.HashTagRepository;
import pl.maciejdebowski.repository.UserRepository;
import pl.maciejdebowski.service.EntryService;
import pl.maciejdebowski.service.UserService;

import javax.jws.soap.SOAPBinding;

import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PersistenceConfig.class)
public class WebApplicationTest {

    @Autowired
    EntryRepository entryRepository;

    @Autowired
    HashTagRepository hashTagRepository;

    @Autowired
    UserRepository userRepository;

    EntryService entryService;

    @Before
    public void before() {
        entryService = new EntryService();
        entryService.setEntryRepository(entryRepository);
        entryService.setHashTagRepository(hashTagRepository);
    }

    @Test
    public void testSqlData(){
        List<User> result = userRepository.findAll();
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void testEntriesAlreadyInDb() {
        Assert.assertEquals(17, entryService.findAll().size());
    }

    @Test
    public void testQueryEntries() {
        Assert.assertEquals(17, entryService.findWithPredicates(null, null, "adam", null));
    }

}