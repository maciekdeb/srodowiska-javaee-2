TODO

1. Dodac zasady dostepu do poszczegolnych miejsc w aplikacji. isAnonymous(), isAuthenticated, hasRole(ROLE_ADMIN)
2. Paginacja

anonymous:
    /query                          POST GET
    /                               GET
    /login                          GET
    /entry/{entryId}                GET
    /registration                   GET POST
    /registration/{confirmationId}  GET

USER:
    /changeInfo                     POST GET
    /changePassword                 POST GET
    /addComment                     POST

EDITOR:
    /edit/entry/{entryId}           GET
    /edit/entry/                    POST
    /create/entry                   POST GET

ADMIN:
    /roles/list                     GET
    /roles/create                   POST GET
    /roles/edit/{name}              GET
    /roles/edit                     POST
    /roles/delete/{id}              GET
    /users/list                     GET
    /users/delete/{id}              GET
